#define 3 variables (set in the terraform.tfvars file)
variable "aws_access_key" {
  description = "holds the access key id"
}

variable "aws_secret_key" {
  description = "holds the secret key"
}

variable "aws_region" {
  default     = "us-west-2"
  description = "sets the default region into e.g Oregon"
}
#create a provider
provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

#create 2 identical instances called web (vpcid and subnet get from aws of the manually created instance)
resource "aws_instance" "web" {
  ami           = "ami-08d489468314a58df" #ami names change per region - this is oregon 
  instance_type = "t2.micro"

  # subnet_id = "sg-07453654b3fa7448a"
  # vpc_security_group_ids = ["vpc-7c528404"]
  count = "2"

  tags = {
    Name = "Student06TF"
  }
}

#2 outputs are in outputs.tf to print the public_ip and public_dns of the 2 web servers
