#!/bin/bash

cd~
sudo yum install -y zip wget unzip git

wget https://releases.hashicorp.com/terraform/0.11.14/terraform_0.11.14_linux_amd64.zip

unzip terraform_0.11.14_linux_amd64.zip

sudo mv terraform /usr/local/bin/
terraform --version

