#this file just separates the outputs from the main.tf - they could go in there as Terraform combines all .tf files on run time but separate them here to make main.tf less busy. All these outputs do is find the public_ip and public_dns details for the 2 web resources (see main.tf we created 2 of the same) the * (splat operator) is used to say get it for both. if just had one it could just be web.upblic_ip

output "public_ip" {
  value = join(", ", aws_instance.web.*.public_ip)
}

output "address" {
  value = join(", ", aws_instance.web.*.public_dns)
}

