# Uncomment the resources below and add the required arguments.

resource "aws_security_group" "student0605" {
    description="lalala"
    vpc_id="vpc-7c528404"
    name = "student0605"
}

resource "aws_security_group_rule" "ssh_ingress_access" {
type = "ingress"
from_port = 22
to_port = 443
protocol = "tcp"
security_group_id = "${aws_security_group.student0605.id}"
cidr_blocks = [ "0.0.0.0/0" ] 
}

resource "aws_security_group_rule" "egress_access" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  security_group_id = "${aws_security_group.student0605.id}"
  cidr_blocks = [ "0.0.0.0/0" ]
}

resource "aws_instance" "tabis_instance" {
  subnet_id = "subnet-88e2c1f1"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.student0605.id}"]
  associate_public_ip_address = true
  user_data = "${file("../shared/user-data.txt")}"
  tags {
    Name = "tabis_instance"
  }
  ami = "ami-08d489468314a58df"
  availability_zone = "us-west-2b"
}

