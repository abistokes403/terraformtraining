#!/bin/bash
#install helpers 

sudo yum install git –y  # the –y sends the yes for you. If was running this in a script you would need the –y to pass in the yes  
sudo yum install tree –y 
sudo yum install mc –y 
 

#install python3 (minimum needed for ansible is python 2.6 or 2.7, must be a linux/unix box) 

sudo yum install python3 –y 
 
#install pip 

sudo yum -y install python3-pip 

sudo pip3 install --upgrade pip 
 
#Install ansible using pip 

pip3 install ansible --user

