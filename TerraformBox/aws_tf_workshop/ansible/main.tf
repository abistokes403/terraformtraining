resource "aws_security_group" "ansible" {
    description="lalala"
    vpc_id="vpc-7c528404"
    name = "ansible"
}
resource "aws_security_group_rule" "ssh_ingress_access" {
type = "ingress"
from_port = 0
to_port = 65535
protocol = "tcp"
security_group_id = "${aws_security_group.ansible.id}"
cidr_blocks = [ "0.0.0.0/0" ]
}
resource "aws_security_group_rule" "egress_access" {
  type = "egress"
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  security_group_id = "${aws_security_group.ansible.id}"
  cidr_blocks = [ "0.0.0.0/0" ]
}
resource "aws_instance" "ans" {
  ami = "ami-087c2c50437d0b80d"
  subnet_id = "subnet-88e2c1f1"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.ansible.id}"]
  associate_public_ip_address = true
  user_data = "${file("../shared/user-data.txt")}"
  tags = {
    Name = "ansible_ch_student0605"
  }
  availability_zone = "us-west-2b"
}
resource "aws_instance" "node" {
  subnet_id = "subnet-88e2c1f1"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.ansible.id}"]
  associate_public_ip_address = true
  user_data = "${file("../shared/user-data.txt")}"
  tags = {
    Name = "node_${count.index +1}_student0605"
  }
  count="2"
  ami = "ami-087c2c50437d0b80d"
  availability_zone = "us-west-2b"
}
resource "aws_instance" "node_3_student0605" {
  subnet_id = "subnet-88e2c1f1"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.ansible.id}"]
  associate_public_ip_address = true
  user_data = "${file("../shared/user-data.txt")}"
  tags = {
    Name = "node_3_student0605"
  }
  ami = "ami-06f2f779464715dc5"
  availability_zone = "us-west-2b"
}

