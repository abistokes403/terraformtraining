resource "aws_cloudwatch_metric_alarm" "cpu_high_alarm" {
  alarm_name = "tabis_40_alarm"
  alarm_description = "This alarm triggers when CPU load in Autoscaling group is high."
 
  metric_name = "tabis_40_metric"
  namespace = "AWS/EC2"
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.tabis_autoscale.name}"
  }
  statistic = "Average"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  period = 120
  threshold = 80
  alarm_actions = ["${aws_autoscaling_policy.autoscale_group_policy_up_x1.arn}"]
}
