resource "aws_launch_configuration" "tabis_config" {
  security_groups = ["${aws_security_group.student06abi.id}","${aws_security_group.student05tony.id}"]
  user_data = "${file("../shared/user-data.txt")}"
  lifecycle { create_before_destroy = true }
  instance_type = "t2.micro"
  image_id = "ami-08d489468314a58df"
}

resource "aws_autoscaling_group" "tabis_autoscale" {
  name = "tabis_autoscale"
  min_size = 1
  max_size = 3
  launch_configuration = "${aws_launch_configuration.tabis_config.name}"
  default_cooldown = 60
  availability_zones = [ "${var.availability_zone_id}" ]
  vpc_zone_identifier = [ "${var.subnet_id}" ]

  tag {
    key = "foo"
    value = "terraform_workshop_student0605"
    propagate_at_launch = true
  }

  lifecycle { create_before_destroy = true }
}

resource "aws_autoscaling_policy" "autoscale_group_policy_up_x1" {
  name = "autoscale_group_policy_up_x1"
  scaling_adjustment = 1
  adjustment_type = "ChangeInCapacity"
  cooldown = 30
  autoscaling_group_name = "${aws_autoscaling_group.tabis_autoscale.name}"
}

